\documentclass[a4paper,french,titlepage]{article}

\usepackage[override,usenames,table]{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{gitinfo2}
\usepackage[french]{babel}
\usepackage[pdftex]{graphicx}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage[french]{varioref}
\usepackage[bottom=2.5cm,top=3cm]{geometry}
\usepackage{lipsum}
\usepackage{svg}

\title{\includegraphics[width=10cm]{insigne.pdf}\\\vspace{4cm}Charte
  graphique de la webradio \texttt{radio6mic}}

\author{Grégory DAVID \and Magali OUISSE}

\date{version \texttt{\gitFirstTagDescribe{}}\\\gitAuthorIsoDate{}}

\begin{document}
\maketitle{}
\vspace*{\fill}
\tableofcontents{}
\vspace*{\fill}

\clearpage%
\begin{figure}[p]
  \centering%
  \includegraphics{logotype.pdf}
  \caption{Logotype de base}
  \label{fig:logotype.base}
\end{figure}

\begin{figure}[p]
  \centering%
  \includegraphics{embleme.pdf}
  \caption{Emblême de base}
  \label{fig:embleme.base}
\end{figure}

\begin{figure}[p]
  \centering%
  \includegraphics{insigne.pdf}
  \caption{Insigne composite des logotype et emblême}
  \label{fig:insigne.base}
\end{figure}


\clearpage%
\section{Éléments graphiques}
\label{sec:elements.graphiques}
\subsection{Logotype}
\label{sec:logotype}
Voir la \figurename~\vref{fig:logotype.base}.

\subsubsection{Positions}
\label{sec:logotype.positions}
\begin{itemize}
  \item Le sommet du chiffre \emph{6} est aligné avec le sommet
  visible des caractères \emph{d} et du \emph{i} du mot \emph{radio}.
  
  \item La gauche visible du chiffre \emph{6} est alignée avec la
  partie droite visible du caractère \emph{o} du mot \emph{radio}.
  
  \item La gauche visible du caractère \emph{m} du mot \emph{mic} est
  alignée avec la partie droite visible du chiffre \emph{6}.

  \item La base des caractères \emph{m} et \emph{i} du mot \emph{mic}
  est alignée avec la base visible du chiffre \emph{6}.

  \item La base des caractères \emph{.} et \emph{n} du mot \emph{.net}
  est alignée avec la base visible du caractère \emph{c} du mot
  \emph{mic}.
\end{itemize}

\subsubsection{Zone d'exclusion}
\label{sec:logotype.zone.exclusion}
La zone d'exclusion s'étend à la hauteur du mot \emph{radio} au-dessus
et en-dessous ; la largeur d'un caractère \emph{a} du mot \emph{radio}
à droite et à gauche.

\subsubsection{Déclinaisons}
\label{sec:logotype.declinaisons}
La \figurename~\vref{fig:logotype.declinaisons} présente les
déclinaisons possibles pour cet élément graphique.

\begin{figure}[hp]
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \includegraphics{logotype.pdf} \subcaption{Trait noir depuis le
      fond lumineux à 100\% \ldots}%
    \label{fig:logotype.declinaisons.cent}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black!20}{\includegraphics{logotype.pdf}}
    \subcaption{\ldots\ jusqu'au fond lumineux à 80\%}%
    \label{fig:logotype.declinaisons.huitante}
  \end{minipage}

  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black!30}{\includegraphics{logotype-invert.pdf}}
    \subcaption{Trait blanc depuis le fond lumineux à 70\% \ldots}%
    \label{fig:logotype.declinaisons.invert.septante}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black}{\includegraphics{logotype-invert.pdf}}
    \subcaption{\ldots\ jusqu'au fond lumineux à 0\%}%
    \label{fig:logotype.declinaisons.invert.zero}
  \end{minipage}

  \caption{Déclinaisons du logotype en fonction de la luminosité de
    l'arrière plan}
  \label{fig:logotype.declinaisons}
\end{figure}

\subsubsection{Interdictions}
\label{sec:logotype.interdictions}
Toute modification de la teinte de fond des mots est interdite --
hormis celles présentées dans la
\figurename~\vref{fig:logotype.declinaisons} : voir
\figurename~\vref{fig:logotype.forbid.grayscale}.

Toute utilisation d'un contour (quelque soit son épaisseur) sur les
mots est interdite : voir
\figurename~\vref{fig:logotype.forbid.outline}.

\begin{figure}[hp]
  \begin{minipage}[b]{1.0\linewidth}
    \centering%
    \includegraphics{logotype-forbid-grayscale-70.pdf}
    \includegraphics{logotype-forbid-grayscale-40.pdf}
    \subcaption{Fond des mots en nuances de gris}%
    \label{fig:logotype.forbid.grayscale}
  \end{minipage}
  
  \begin{minipage}[b]{1.0\linewidth}
    \centering%
    \colorbox{black}{\includegraphics{logotype-forbid-outline-white.pdf}}
    \includegraphics{logotype-forbid-outline-black.pdf}
    \subcaption{Contours des mots quelques soient les situations}%
    \label{fig:logotype.forbid.outline}
  \end{minipage}
  \caption{Interdictions}
  \label{fig:logotype.interdictions}
\end{figure}

\clearpage%
\subsection{Emblême}
\label{sec:embleme}
Voir la \figurename~\vref{fig:embleme.base}.

\subsubsection{Déclinaisons}
\label{sec:embleme.declinaisons}
La \figurename~\vref{fig:embleme.declinaisons} présente les
déclinaisons possibles pour cet élément graphique.

\begin{figure}[h!]
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \includegraphics{embleme.pdf} \subcaption{Trait noir depuis le
      fond lumineux à 100\% \ldots}%
    \label{fig:embleme.declinaisons.cent}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black!20}{\includegraphics{embleme.pdf}}
    \subcaption{\ldots\ jusqu'au fond lumineux à 80\%}%
    \label{fig:embleme.declinaisons.huitante}
  \end{minipage}

  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black!30}{\includegraphics{embleme-invert.pdf}}
    \subcaption{Trait blanc depuis le fond lumineux à 70\% \ldots}%
    \label{fig:embleme.declinaisons.invert.septante}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black}{\includegraphics{embleme-invert.pdf}}
    \subcaption{\ldots\ jusqu'au fond lumineux à 0\%}%
    \label{fig:embleme.declinaisons.invert.zero}
  \end{minipage}

  \caption{Déclinaisons de l'emblême en fonction de la luminosité de
    l'arrière plan}
  \label{fig:embleme.declinaisons}
\end{figure}

\subsubsection{Interdictions}
\label{sec:embleme.interdictions}
Toute modification de la teinte de fond des courbes est interdite --
hormis celles présentées dans la
\figurename~\vref{fig:embleme.declinaisons} : voir
\figurename~\vref{fig:embleme.forbid.grayscale}.

Toute utilisation d'un contour (quelque soit son épaisseur) sur les
courbes est interdite : voir
\figurename~\vref{fig:embleme.forbid.outline}.

\begin{figure}[h!]
  \begin{minipage}[b]{1.0\linewidth}
    \centering%
    \includegraphics{embleme-forbid-grayscale-70.pdf}
    \includegraphics{embleme-forbid-grayscale-40.pdf} \subcaption{Fond
      des courbes en nuances de gris}%
    \label{fig:embleme.forbid.grayscale}
  \end{minipage}
  
  \begin{minipage}[b]{1.0\linewidth}
    \centering%
    \colorbox{black}{\includegraphics{embleme-forbid-outline-white.pdf}}
    \includegraphics{embleme-forbid-outline-black.pdf}
    \subcaption{Contours des courbes}%
    \label{fig:embleme.forbid.outline}
  \end{minipage}
  \caption{Interdictions}
  \label{fig:embleme.interdictions}
\end{figure}

\clearpage%
\subsection{Insigne}
\label{sec:insigne}
Voir la \figurename~\vref{fig:insigne.base}.

\subsubsection{Déclinaisons}
\label{sec:insigne.declinaisons}
La \figurename~\vref{fig:insigne.declinaisons} présente les
déclinaisons possibles pour cet élément graphique.

\begin{figure}[h!]
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \includegraphics{insigne.pdf} \subcaption{Trait noir depuis le
      fond lumineux à 100\% \ldots}%
    \label{fig:insigne.declinaisons.cent}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black!20}{\includegraphics{insigne.pdf}}
    \subcaption{\ldots\ jusqu'au fond lumineux à 80\%}%
    \label{fig:insigne.declinaisons.huitante}
  \end{minipage}

  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black!30}{\includegraphics{insigne-invert.pdf}}
    \subcaption{Trait blanc depuis le fond lumineux à 70\% \ldots}%
    \label{fig:insigne.declinaisons.invert.septante}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \centering%
    \colorbox{black}{\includegraphics{insigne-invert.pdf}}
    \subcaption{\ldots\ jusqu'au fond lumineux à 0\%}%
    \label{fig:insigne.declinaisons.invert.zero}
  \end{minipage}

  \caption{Déclinaisons de l'emblême en fonction de la luminosité de
    l'arrière plan}
  \label{fig:insigne.declinaisons}
\end{figure}

\subsubsection{Interdictions}
\label{sec:insigne.interdictions}
Toute modification de la teinte des fonds est interdite -- hormis
celles présentées dans la \figurename~\vref{fig:insigne.declinaisons}
: voir \figurename~\vref{fig:insigne.forbid.grayscale}.

Toute utilisation d'un contour (quelque soit son épaisseur) sur les
éléments textuels ou graphiques est interdite : voir
\figurename~\vref{fig:insigne.forbid.outline}.

\begin{figure}[h!]
  \begin{minipage}[b]{1.0\linewidth}
    \centering%
    \includegraphics{insigne-forbid-grayscale-70.pdf}
    \includegraphics{insigne-forbid-grayscale-40.pdf} \subcaption{Fond
      des éléments en nuances de gris}%
    \label{fig:insigne.forbid.grayscale}
  \end{minipage}
  
  \begin{minipage}[b]{1.0\linewidth}
    \centering%
    \colorbox{black}{\includegraphics{insigne-forbid-outline-white.pdf}}
    \includegraphics{insigne-forbid-outline-black.pdf}
    \subcaption{Contours des éléments}%
    \label{fig:insigne.forbid.outline}
  \end{minipage}
  \caption{Interdictions}
  \label{fig:insigne.interdictions}
\end{figure}

\clearpage%
\section{Polices de caractères}
\label{sec:polices.de.caracteres}

Polices de caractères utilisées et relations de tailles entre les
éléments.

\subsection{Pour les mots \texttt{radio} et \texttt{mic}}
\begin{description}
  \item[Nom] Nimbus Sans L
  \item[Style] Bold
  \item[Type] police PostScript Type 1
  \item[Taille] $8.98930931px$
  \item[Interligne] $1.25$
  \item[Espacement inter caractères] 0
  \item[Espacement inter mots] 0
  \item[Justification] Droite
  \item[Transformations] ~
  \begin{description}
    \item[Aucune] ~
  \end{description}
\end{description}

\subsection{Pour le chiffre \texttt{6}}
\begin{description}
  \item[Nom] Nimbus Sans L
  \item[Style] Bold
  \item[Type] police PostScript Type 1
  \item[Taille] $10.53357697px$
  \item[Interligne] $1.25$
  \item[Espacement inter caractères] 0
  \item[Espacement inter mots] 0
  \item[Justification] Gauche
  \item[Transformations] ~
  \begin{description}
    \item[\emph{skew X}] $-14.242904$
  \end{description}
\end{description}

\subsection{Pour le mot \texttt{.net}}
\begin{description}
  \item[Nom] Nimbus Sans L
  \item[Style] Bold
  \item[Type] police PostScript Type 1
  \item[Taille] $3.54005718px$
  \item[Interligne] $1.25$
  \item[Espacement inter caractères] 0
  \item[Espacement inter mots] 0
  \item[Justification] Gauche
  \item[Transformations] ~
  \begin{description}
    \item[Aucune] ~
  \end{description}
\end{description}

\clearpage%
\section{Jeux de couleurs}
\label{sec:jeux.de.couleurs}

\subsection{Sur fond clair}
\label{sec:jeux.de.couleurs.fond.clair}

\begin{table}[h!]
  \centering%
  \begin{tabular}{lllll}
    \hline
    Élément & Pantone & CMJN & RVB & Hexadécimale \\
    \hline
    \texttt{radio} & Black & (75, 68, 65, 82) & (0, 0, 0) & \#000000 \\
    \texttt{6} & Black & (75, 68, 65, 82) & (0, 0, 0) & \#000000 \\
    \texttt{mic} & Black & (75, 68, 65, 82) & (0, 0, 0) & \#000000 \\
    \texttt{.net} & Black & (75, 68, 65, 82) & (0, 0, 0) & \#000000 \\
    \hline
  \end{tabular}
  \caption[Couleurs du logotype]{Jeux de couleurs pour les éléments du
    logotype sur fond clair}
  \label{tab:couleurs.fond.clair.logotype}
\end{table}

\begin{table}[h!]
  \centering%
  \begin{tabular}{lllll}
    \hline
    Élément & Pantone & CMJN & RVB & Hexadécimale \\
    \hline
    Volcan & Black & (75, 68, 65, 82) & (0, 0, 0) & \#000000 \\
    Signal & Black & (75, 68, 65, 82) & (0, 0, 0) & \#000000 \\
    \hline
  \end{tabular}
  \caption[Couleurs de l'emblême]{Jeux de couleurs pour les éléments
    de l'emblême sur fond clair}
  \label{tab:couleurs.fond.clair.embleme}
\end{table}

\subsection{Sur fond sombre}
\label{sec:jeux.de.couleurs.fond.sombre}

\begin{table}[h!]
  \centering%
  \begin{tabular}{lllll}
    \hline
    Élément & Pantone & CMJN & RVB & Hexadécimale \\
    \hline
    \texttt{radio} & 705 & (0, 0, 0, 0) & (255, 255, 255) & \#FFFFFF \\
    \texttt{6} & 705 & (0, 0, 0, 0) & (255, 255, 255) & \#FFFFFF \\
    \texttt{mic} & 705 & (0, 0, 0, 0) & (255, 255, 255) & \#FFFFFF \\
    \texttt{.net} & 705 & (0, 0, 0, 0) & (255, 255, 255) & \#FFFFFF \\
    \hline
  \end{tabular}
  \caption[Couleurs du logotype]{Jeux de couleurs pour les éléments du
    logotype sur fond sombre}
  \label{tab:couleurs.logotype}
\end{table}

\begin{table}[h!]
  \centering%
  \begin{tabular}{lllll}
    \hline
    Élément & Pantone & CMJN & RVB & Hexadécimale \\
    \hline
    Volcan & 705 & (0, 0, 0, 0) & (255, 255, 255) & \#FFFFFF \\
    Signal & 705 & (0, 0, 0, 0) & (255, 255, 255) & \#FFFFFF \\
    \hline
  \end{tabular}
  \caption[Couleurs de l'emblême]{Jeux de couleurs pour les éléments
    de l'emblême sur fond sombre}
  \label{tab:couleurs.embleme}
\end{table}

\end{document}
